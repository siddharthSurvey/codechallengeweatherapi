package com.example.codechallengeweatherapi.Dashboard

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.codechallengeweatherapi.DB.City
import com.example.codechallengeweatherapi.R
import kotlinx.android.synthetic.main.row_city_list.view.*

class CityListAdapter(private val context: Context, private val cityList: ArrayList<City>, private val cityManagementListener: CityManagementListener) :
        RecyclerView.Adapter<CityListAdapter.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {

        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row_city_list, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {

        holder.txtCityName.text = cityList[position].city

        holder.bind(cityList[position])
    }

    override fun getItemCount(): Int
    {
        return cityList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var txtCityName: TextView = itemView.txtCityName
        var imgDelete:ImageView =itemView.imgDelete

        fun bind(city: City){
            itemView.setOnClickListener {
                cityManagementListener.onCitySelected(city)
            }
            imgDelete.setOnClickListener {
                cityManagementListener.onCityDeleted(city)
            }
        }
    }

}