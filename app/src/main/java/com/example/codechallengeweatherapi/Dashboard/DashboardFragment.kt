package com.example.codechallengeweatherapi.Dashboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codechallengeweatherapi.DB.City
import com.example.codechallengeweatherapi.R
import com.example.codechallengeweatherapi.databinding.DashboardFragmentBinding
import kotlinx.android.synthetic.main.dashboard_fragment.*

class DashboardFragment : Fragment(), CityManagementListener {

    companion object {
        fun newInstance() = DashboardFragment()
    }

    private lateinit var viewModel: DashboardViewModel
    private lateinit var dataBinding: DashboardFragmentBinding
    private var cityListNew = arrayListOf<City>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        dataBinding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container,false)
        dataBinding.lifecycleOwner = this

        dataBinding.apply {

            fabMap.setOnClickListener {
                findNavController().navigate(R.id.action_dashboardFragment_to_mapsFragment)
            }

        }

        viewModel.apply {
            allCity.observe(viewLifecycleOwner, Observer { cityList ->
                if(cityList.isEmpty()){
                    Toast.makeText(activity, "No city entered", Toast.LENGTH_SHORT).show()
                }else{
                    dataBinding.let {
                        cityListNew = cityList as ArrayList<City>
                        val adapter = activity?.let { CityListAdapter(it, cityListNew, this@DashboardFragment) }
                        rcvCityList.adapter = adapter
                        rcvCityList.layoutManager = LinearLayoutManager(activity)
                    }
                }
            })
        }

        return dataBinding.root
    }

    override fun onCityDeleted(city: City) {
        viewModel.deleteCity(city)
        cityListNew.remove(city)
        dataBinding.rcvCityList.adapter?.notifyDataSetChanged()
    }

    override fun onCitySelected(city: City) {
        findNavController().navigate(R.id.action_dashboardFragment_to_cityForecastFragment,
            Bundle().apply {
                putString("city", city.city)
                putString("fromWhere", "dashboard")
            })
    }

}