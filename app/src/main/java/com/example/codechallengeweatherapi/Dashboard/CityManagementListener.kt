package com.example.codechallengeweatherapi.Dashboard

import com.example.codechallengeweatherapi.DB.City

interface CityManagementListener {
    fun onCityDeleted(city: City)
    fun onCitySelected(city: City)
}