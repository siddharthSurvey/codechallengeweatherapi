package com.example.codechallengeweatherapi.Dashboard

import android.app.Application
import android.app.homversity.restApis.Coroutines
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.codechallengeweatherapi.DB.City
import com.example.codechallengeweatherapi.DB.CityRepository
import com.example.codechallengeweatherapi.DB.CityRoomDatabase

class DashboardViewModel(application: Application) : AndroidViewModel(application) {
    private val cityRepository: CityRepository
    val allCity: LiveData<List<City>>

    init {
        val cityDao = CityRoomDatabase.getDatabase(application).cityDao()
        cityRepository = CityRepository(cityDao)
        allCity = cityRepository.allWords
    }

    fun deleteCity(city: City) = Coroutines.io {
        cityRepository.deleteCity(city)
    }

}