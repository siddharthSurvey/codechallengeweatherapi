package com.example.codechallengeweatherapi.RestApis

object Constants
{
    var BASE_API = "https://api.openweathermap.org/data/2.5/"
    const val API_KEY = "fae7190d7e6433ec3a45285ffcf55c86"
    const val GET_CURRENT_WEATHER = "weather"
    const val GET_FORECAST = "forecast"
    const val SERVER_SUCCESS_CODE=200

}