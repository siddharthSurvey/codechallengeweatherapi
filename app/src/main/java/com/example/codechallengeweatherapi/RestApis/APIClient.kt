package android.app.homversity.restApis

import com.example.codechallengeweatherapi.dataclass.CurrentWeather
import com.example.codechallengeweatherapi.RestApis.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIClient
{
    private val apiInterface: APIInterface

    init
    {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        apiInterface = Retrofit.Builder()
                .baseUrl(Constants.BASE_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIInterface::class.java)
    }

   suspend fun getCurrentWeather(cityName: String?, apiKey: String?): Response<CurrentWeather?>?
    {
        return apiInterface.getCurrentWeather(cityName, apiKey)
    }
}