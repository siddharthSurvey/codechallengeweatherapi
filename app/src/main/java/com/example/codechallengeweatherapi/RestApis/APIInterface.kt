package android.app.homversity.restApis

import com.example.codechallengeweatherapi.dataclass.CurrentWeather
import com.example.codechallengeweatherapi.RestApis.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface APIInterface {

   @Headers("Accept:application/json")
   @GET(Constants.GET_CURRENT_WEATHER)
   suspend fun getCurrentWeather(@Query("q") city_name: String?,
                                 @Query("appid") key: String?): Response<CurrentWeather?>?

    @Headers("Accept:application/json")
    @GET(Constants.GET_FORECAST)
    suspend fun getForecast(@Query("q") city_name: String?,
                                  @Query("appid") key: String?): Response<CurrentWeather?>?

}