package com.example.codechallengeweatherapi.city

import android.app.Application
import android.app.homversity.restApis.APIClient
import android.app.homversity.restApis.Coroutines
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.codechallengeweatherapi.DB.City
import com.example.codechallengeweatherapi.DB.CityRepository
import com.example.codechallengeweatherapi.DB.CityRoomDatabase
import com.example.codechallengeweatherapi.dataclass.CurrentWeather
import com.example.codechallengeweatherapi.RestApis.Constants

class CityForecastViewModel(application: Application) : AndroidViewModel(application) {
    // TODO: Implement the ViewModel

    var cityName = MutableLiveData<String>()
    var cityWeatherResponse = MutableLiveData<CurrentWeather>()
    var cityWeatherFailResponse =  MutableLiveData<String>()
    private val cityRepository: CityRepository
    val allCity: LiveData<List<City>>

    init {
        val cityDao = CityRoomDatabase.getDatabase(application).cityDao()
        cityRepository = CityRepository(cityDao)
        allCity = cityRepository.allWords
    }

    fun insertCity(city: City) = Coroutines.io {
        cityRepository.insert(city)
    }

    fun deleteCity(city: City) = Coroutines.io {
        cityRepository.deleteCity(city)
    }

    fun getCityWeather(selectedCity: String)
    {
        Coroutines.main {
            val response = APIClient.getCurrentWeather(selectedCity, Constants.API_KEY)

            if (response?.code() == Constants.SERVER_SUCCESS_CODE)
            {
                cityWeatherResponse.value=response.body()
            }
            else
            {
                cityWeatherFailResponse.value=response?.message()
            }
        }
    }



}