package com.example.codechallengeweatherapi.city

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.codechallengeweatherapi.DB.City
import com.example.codechallengeweatherapi.R
import com.example.codechallengeweatherapi.databinding.CityForecastFragmentBinding
import com.example.codechallengeweatherapi.dataclass.CurrentWeather
import kotlin.math.roundToInt

class CityForecastFragment : Fragment() {

    companion object {
        fun newInstance() = CityForecastFragment()
    }

    private lateinit var viewModel: CityForecastViewModel
    private lateinit var databinding: CityForecastFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        databinding = DataBindingUtil.inflate(inflater, R.layout.city_forecast_fragment, container,false)
        databinding.lifecycleOwner = this

        viewModel = ViewModelProvider(this).get(CityForecastViewModel::class.java)
        databinding.viewModel1 = viewModel

        val city = arguments?.getString("city").toString()
        viewModel.cityName.value = city
        val fromWhere = arguments?.getString("fromWhere").toString()

        databinding.rbBookmarkCity.isChecked = fromWhere.equals("dashboard", true)

        databinding.apply {

            rbBookmarkCity.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if(isChecked){
                        viewModel.insertCity(City(city, true))

                    }else{
                        viewModel.deleteCity(City(city, false))

                    }
                }
            })
        }

        viewModel.apply {

            cityName.observe(viewLifecycleOwner, Observer {city ->
                databinding.progressBar.visibility  = View.VISIBLE
                getCityWeather(city)
            })
            allCity.observe(viewLifecycleOwner, Observer {cityList ->
                cityList.forEach {
                    if(it.city == city){
                        if(it.isBookmarked)
                            databinding.rbBookmarkCity.isChecked = true
                    }
                }
            })
            cityWeatherResponse.observe(viewLifecycleOwner, Observer {
                databinding.progressBar.visibility  = View.GONE
                setWeatherData(it)
            })
            cityWeatherFailResponse.observe(viewLifecycleOwner, Observer {
                Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
            })
        }
        return databinding.root
    }

    private fun setWeatherData(currentWeather: CurrentWeather){
        databinding.apply {
            txtTempreture.text = "${(currentWeather.main.temp - 273.15).roundToInt()} ℃"
            val minTemp = currentWeather.main.temp_min - 273.15
            val maxTemp = currentWeather.main.temp_max - 273.15
            txtMinMaxTemp.text = "Min/Max \n ${minTemp.roundToInt()}℃/${maxTemp.roundToInt()}℃"

            val degree = currentWeather.wind.deg
            val windDirection = degree.let {
                when{
                    degree>337.5 ->  "Northerly"
                    degree>292.5 ->  "North Westerly"
                    degree>247.5 -> "Westerly"
                    degree>202.5 ->  "South Westerly"
                    degree>157.5 -> "Southerly"
                    degree>122.5 ->  "South Easterly"
                    degree>67.5 ->  "Easterly"
                    degree>22.5 ->   "North Easterly"
                    else -> "Northerly"
                }
            }

            txtWindDirection.text = "Wind Direction: $windDirection"
            txtWindSpeed.text = "Wind Speed: ${currentWeather.wind.speed}kms/h"
            txtPressure.text = "Pressure: ${currentWeather.main.pressure} hPa"
            txtVisibility.text = "Visibility:  ${currentWeather.visibility/1000} kms"
            txtHumidity.text = "Humidity: ${currentWeather.main.humidity}%"
        }
    }

}