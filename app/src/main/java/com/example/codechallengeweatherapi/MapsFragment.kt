package com.example.codechallengeweatherapi

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import java.util.*


class MapsFragment : Fragment() {
    val MY_PERMISSIONS_REQUEST_LOCATION = 99
    lateinit var googleMapG: GoogleMap
    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */

        /*val sydney = LatLng(-34.0, 151.0)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))*/
        googleMapG = googleMap
        zoomToCurrentCity(googleMap)
        googleMap.setOnMapClickListener { it ->
            val gcd = Geocoder(context, Locale.getDefault())
            val addresses: List<Address> = gcd.getFromLocation(it.latitude, it.longitude, 1)
            if (addresses.isNotEmpty()) {
                println(addresses[0].locality)
                if(addresses[0].locality != null) {
//                    Toast.makeText(activity, addresses[0].locality, Toast.LENGTH_SHORT).show()

                    findNavController().navigate(R.id.action_mapsFragment_to_cityForecastFragment,
                        Bundle().apply {
                            putString("city", addresses[0].locality)
                            putString("fromWhere", "maps")
                        })
                }else{
                    Toast.makeText(activity, "Please try again", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(activity, "Please try again", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun zoomToCurrentCity(googleMap: GoogleMap){
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                requireActivity(), arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), MY_PERMISSIONS_REQUEST_LOCATION
            )
        } else {
            googleMap.isMyLocationEnabled = true;
            var locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager;
            var criteria =  Criteria()
            var provider = locationManager.getBestProvider(criteria, true);
            var location = locationManager.getLastKnownLocation(provider.toString());


            if (location != null) {
                var latitude = location.getLatitude();
                var longitude = location.getLongitude();
                var latLng =  LatLng(latitude, longitude);
                var myPosition =  LatLng(latitude, longitude)


                var coordinate =  LatLng(latitude, longitude);
                var yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 19f);
                googleMap.animateCamera(yourLocation);
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (permissions[0] == Manifest.permission.ACCESS_COARSE_LOCATION
                && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                zoomToCurrentCity(googleMapG);
            }else{
                zoomToCurrentCity(googleMapG)
            }
        }
    }

}