package com.example.codechallengeweatherapi.DB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DBConstants.TABLE_CITY)
class City(@PrimaryKey @ColumnInfo(name = DBConstants.COLUMN_CITY) val city: String,
           @ColumnInfo(name = DBConstants.COLUMN_CITY_IS_BOOKMARKED) val isBookmarked: Boolean)