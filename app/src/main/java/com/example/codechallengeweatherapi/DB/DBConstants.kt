package com.example.codechallengeweatherapi.DB

object DBConstants {
    const val DB_NAME = "city_database"
    const val TABLE_CITY = "city_table"
    const val COLUMN_CITY = "city"
    const val COLUMN_CITY_IS_BOOKMARKED = "is_bookmarked"
    const val COLUMN_ID = "id"
}