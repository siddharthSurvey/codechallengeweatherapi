package com.example.codechallengeweatherapi.DB

import androidx.lifecycle.LiveData

class CityRepository(private val cityDao: CityDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allWords: LiveData<List<City>> = cityDao.getAllCity()
 
    suspend fun insert(city: City) {
        cityDao.insert(city)
    }

    suspend fun deleteCity(city: City) {
        cityDao.deleteCity(city.city)
    }
}