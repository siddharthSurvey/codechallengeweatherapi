package com.example.codechallengeweatherapi.DB

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CityDao {

    @Query("SELECT * FROM ${DBConstants.TABLE_CITY} ORDER BY ${DBConstants.COLUMN_CITY} ASC")
    fun getAllCity(): LiveData<List<City>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(city: City)

    @Query("DELETE FROM ${DBConstants.TABLE_CITY} WHERE ${DBConstants.COLUMN_CITY} LIKE :city")
    suspend fun deleteCity(city: String)
}